package com.spring.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;

/**
 * @Author Reshma
 * @Date 7/11/2018
 * @Month 07
 **/
@Controller
public class EmailController {
    //static String emailToRecipient, message, emailSubject;
    static String emailFromRecipient="maharjanresu@gmail.com";
    public ModelAndView modelAndView;
    @Autowired
    JavaMailSender javaMailSender;

    @RequestMapping("/")
    public ModelAndView getModelAndView() {
        modelAndView = new ModelAndView("index");
        return modelAndView;
    }

    @RequestMapping(value = "/sendEmail", method = RequestMethod.POST)/*
    public ModelAndView sendEmailToClient(@RequestParam final CommonsMultipartFile attachFileObj,
                                            HttpServletRequest request) {
       emailFromRecipient = request.getParameter("mailTo");
        emailSubject = request.getParameter("subject");
        message = request.getParameter("message");*/

    public ModelAndView sendEmailToClient(@RequestParam("mailTo")final String emailToRecipient,
                                          @RequestParam("subject")final String emailSubject,
                                          @RequestParam("message")final String message ,
                                          @RequestParam final CommonsMultipartFile attachFileObj) {

        javaMailSender.send(new MimeMessagePreparator() {
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
                mimeMessageHelper.setTo(emailToRecipient);
                mimeMessageHelper.setFrom(emailFromRecipient);
                mimeMessageHelper.setText(message);
                mimeMessageHelper.setSubject(emailSubject);

                if (attachFileObj != null && (!attachFileObj.equals("")) && (attachFileObj.getSize() > 0)) {
                    mimeMessageHelper.addAttachment(attachFileObj.getOriginalFilename(), new InputStreamSource() {
                        public InputStream getInputStream() throws IOException {
                            return attachFileObj.getInputStream();
                        }
                    });
                } else {
                    System.out.println("File not selected!!");
                }
            }
        });
        System.out.println("File successfully send");
        modelAndView = new ModelAndView("success", "messageObj", "Email send");
        return modelAndView;
    }
}
